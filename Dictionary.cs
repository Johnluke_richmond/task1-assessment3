using System;
using System.Collections.Generic;

class Program
{
    public void Main()
    {
        Dictionary<string, int> dictionary =
            new Dictionary<string, int>();

        dictionary.Add("January", 31);
        dictionary.Add("Febuary", 28);
        dictionary.Add("March", 31);
        dictionary.Add("April", 30);
        dictionary.Add("May", 31);
        dictionary.Add("June", 30);
        dictionary.Add("July", 31);
        dictionary.Add("August", 31);
        dictionary.Add("September", 30);
        dictionary.Add("October", 31);
        dictionary.Add("November", 30);
        dictionary.Add("December", 31);

        foreach(KeyValuePair<string, int> kvp in dictionary)
        {
            Console.WriteLine("{0}, {31}", kvp.Key, kvp.Value);
        }
    }
}