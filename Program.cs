﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace assessment3_task1
{
    class Program
    {
        static void Main()
        {
          var names = new List<people>();
          var month = new List<DateTime>(); 
          
          names.Add(new people{name = "Chris Paul", month = 11, day = 23});
          names.Add(new people{name = "Carmelo Anthony", month = 8, day = 28});
          names.Add(new people{name = "Kyrie Irving", month = 9, day = 12});
          names.Add(new people{name = "Lebron James", month = 4, day = 14});
          names.Add(new people{name = "Dwyane Wade", month  = 7, day = 24});
          names.Add(new people{name = "Gordon Hayward", month = 3, day = 1});
          names.Add(new people{name = "Kristaps Porzingis", month = 2, day = 11});
          names.Add(new people{name = "Michael Jordan", month = 1, day = 26});
          names.Add(new people{name = "Magic Johnson", month = 4, day = 1});
          names.Add(new people{name = "Larry Bird", month = 10, day = 8});
          names.Add(new people{name = "Kobe Bryant", month = 6, day = 9});


        }     
    }
}
